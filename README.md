# COMP3931Individual Project

## SSD Failure Prediction based on Classification Models and Data Engineering

In order to predict the SSD failure more accurately and reduce the O&M costs, failure prediction methods based on classification models are studied in this project. 

The original dataset can be downloaded from https://tianchi.aliyun.com/dataset/dataDetail?dataId=95044

ssd_failure.py is the code of my work. I use Jupyter as my IDE. For checking the code easier, I transform it from .ipynb to .py


result.csv is the prediction result of my work


