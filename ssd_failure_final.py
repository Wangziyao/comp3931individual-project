#!/usr/bin/env python
# coding: utf-8
# I use jupyter as the IDE. For checking my code easier, I transform my code from .ipynb to .py
# In[1]:


# %%
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from pathlib import Path
from itertools import chain
from tqdm import tqdm,tqdm_notebook
import random
import time
from datetime import datetime
import warnings
warnings.filterwarnings('ignore')


DATA_PATH = Path('/group/DATASET/SMART')

# loading data
df_failure = pd.read_csv(DATA_PATH / 'ssd_failure_label.csv',parse_dates=['failure_time']).sort_values(by='failure_time')
df_failure['date'] = df_failure.failure_time.map(lambda dt:dt.strftime('%Y%m%d'))
df_failure


"""
1. construction of experimental data
- choose six months' data
- select the failure data form df_failure,
  filter them into model+disk_id as the positive label
- select same amount model+disk_id randomly from SMART logs as the negative label
"""


# failure_items: select all failure label from 6 months' data
# num_failure_items = 3541
use_time_min,use_time_max = datetime(2018,7,1),datetime(2018,12,31)
failure_in_use_time_mask = (use_time_min< df_failure.failure_time) * (df_failure.failure_time<=use_time_max)
df_failure_use = df_failure[failure_in_use_time_mask]
failure_items = set([tuple(i) for i in df_failure_use[['disk_id','model']].values])
num_failure_items = len(failure_items)
df_failure_use


# read the data of 01.07.2018, and select num_failure_item from negative label
# load checkpoint
df_2018_7_1 = pd.read_csv(DATA_PATH/'2018'/'20180701.csv')
all_items = set([tuple(i) for i in df_2018_7_1[['disk_id','model']].values])
no_failure_items_all = all_items - failure_items
# get 3541 no failure items by random sample
random.seed(2022)
no_failure_items = random.sample(no_failure_items_all, num_failure_items)
no_failure_items[:10]

# concat failure_items(all) and no_failure_items(random sample)
# len(no_failure_items) = len(failure_items) = 3541,  len(use_items) = 7082
use_items = set(no_failure_items) | failure_items
len(use_items)


# read data from 7.1 to 12.30, select rows which contains no_failure_items and failure_items的数据行
# get the all use data
read_data = sorted([fp for fp in (DATA_PATH/'2018').rglob("*.csv") if ('20180701'<=fp.name.strip('.csv')<='20181230')])

all_use_df = []
for fp in tqdm_notebook(read_data,desc="filter data..."):
    df_tmp = pd.read_csv(fp)
    # find need row
    use_items_mask = [(item[0],item[1]) in use_items for item in df_tmp[['disk_id','model']].values]
    df_tmp = df_tmp[use_items_mask]
    all_use_df.append(df_tmp.copy())
    print(fp, df_tmp.shape)

# df_use
df_use = pd.concat(all_use_df).sort_values(by=['model','disk_id','ds'])

# labeled
# All data backwards failure_label_range days from the date of the failure are labelled 1, the rest are labelled 0
from datetime import timedelta

df_use['label'] = 0
for irow in tqdm_notebook(range(len(df_failure_use)), desc='label raw data...'):
    model, dt, disk_id, _ = df_failure_use.iloc[irow].values
    label1_min = int((dt - timedelta(days=29)).strftime('%Y%m%d'))
    label1_max = int(dt.strftime('%Y%m%d'))

    label_1_mask = (df_use.model == model) * (df_use.disk_id == disk_id) * (df_use.ds >= label1_min) * (
                df_use.ds <= label1_max)
    df_use.loc[label_1_mask, 'label'] = 1

#  ratio of two categories of labeled data = 96:9 ≈ 10:1
df_use.label.value_counts()

# save file
df_use.to_csv('/data/df_raw.csv',index=None)
df_use



"""
2. Feature engineering 
    a. drop empty and almost null features
    b. outiler
    c. missing value imputation
    d. variance filtering: drop feature which std=0
    e. feature selection
    f. feature augmentation
"""
# load checkpoint
df_use=pd.read_csv('/data/df_raw.csv')
df_clean = df_use.copy()
df_clean['ds']=df_clean['ds'].map(lambda x:datetime.strptime(str(x),'%Y%m%d'))

# observe df_clean, there are many null in the data, delete them
df_clean.info(3, null_counts=True)


# In[5]:
# count the variance, and delete the feature with variance==0
df_clean_desc = df_clean.loc[:, 'n_1':'r_232'].describe()
df_clean_desc


# In[7]:


# delete empty and almost null features
null_cols = ['n_2','r_2','n_3','r_3','n_4','r_4','n_6','r_6','n_7','r_7',
             'n_8','r_8','n_10','r_10','n_11','r_11','n_13','r_13','n_189',
             'r_189','n_191','r_191','n_193','r_193','n_200','r_200','n_204',
             'r_204','n_205','r_205','n_207','r_207','n_211','r_211','n_240','r_240']
almost_null_cols = ['n_177', 'r_177', 'n_181', 'r_181', 'n_192', 'r_192', 'n_233', 'r_233', 'n_244', 'r_244', 'n_245', 'r_245']

need_del_cols = set(null_cols) | set(almost_null_cols)
df_clean = df_clean.drop(need_del_cols, 1)
df_clean.shape

# outliers

# In[8]:


# use box_plot(scale=3) to find outliers
def outliers_proc(data, col_name, scale=3):
    """
    outlier cleaning
    param data: pandas
    param col_name: pandas columns name
    param scale:
    """
        
    def box_plot_outliers(data_ser, box_scale):
        """
        find outiers by box plot
        :param data_ser: pandas.Series
        :param box_scale: scale of box plot
        """
        upper = data_ser.quantile(0.75)
        lower = data_ser.quantile(0.25)
        iqr = box_scale * (upper - lower)
        val_low = data_ser.quantile(0.25) - iqr
        val_up = data_ser.quantile(0.75) + iqr
        rule_low = (data_ser < val_low)
        rule_up = (data_ser > val_up)
        return (rule_low,rule_up),(val_low,val_up)
    
    data_n = data.copy()
    data_serier = data_n[col_name]
    rule, value = box_plot_outliers(data_serier,box_scale=scale)
    index = np.arange(data_serier.shape[0])[rule[0]|rule[1]]
    print(index)
    print("Outlier number is:{}".format(len(index)))
    data_n = data_n.drop(index)
    data_n.reset_index(drop=True, inplace=True)
    print("Now column number is:{}".format(data_n.shape[0]))
    index_low = np.arange(data_serier.shape[0])[rule[0]]
    outliers = data_serier.iloc[index_low]
    print("Description of data less than the lower bound is:")
    print(pd.Series(outliers).describe())
    index_up = np.arange(data_serier.shape[0])[rule[1]]
    outliers = data_serier.iloc[index_up]
    print("Description of data larger than the upper bound is:")
#     print(pd.Series(outliers).describe())
    
    fig, ax = plt.subplots(1,2, figsize=(10,7))
    sns.boxplot(y=data[col_name],data=data,palette="Set1", ax=ax[0])
    sns.boxplot(y=data_n[col_name],data=data_n,palette="Set1",ax=ax[1])
    ax[0].set_title('n_1, before')
    ax[1].set_title('n_1, after')
    return data_n


# In[11]:

col_num = df_feature.shape[1]
feats = df_feature.columns
df_feature = df_clean.groupby('ds', as_index=False)[df_clean.columns[3:-1]].mean()
for feat in feats:
    outliers_proc(df_feature,feats,scale=3)
    


# In[10]:

    
for i in range(col_num // 2):
    fig,axes = plt.subplots(1,2)
    feat_n,feat_r = feats[i*2],feats[i*2+1]
    sns.boxplot(ax=axes[0], data=df_feature[feat_n])
    sns.boxplot(ax=axes[1], data=df_feature[feat_r])
    axes[0].set_title(feat_n)
    axes[1].set_title(feat_r)
    


# In[12]:

# count the variance, and delete the feature with variance==0
df_clean_desc = df_clean.loc[:, 'n_1':'r_232'].describe()
df_clean_desc


# In[13]:


# drop features which std=0
mask = (df_clean_desc.loc['std',: ] == 0)
std0_cols = mask[mask == True].index.tolist()
print(std0_cols)
need_del_cols = set(std0_cols)
df_clean = df_clean.drop(need_del_cols, 1)
df_clean.shape


# In[14]:


# All remaining features are averaged by day
# visualization of feature distribution
df_feature_mean = df_clean.groupby('ds')[df_clean.columns[3:-1]].mean()
col_num = df_feature_mean.shape[1]
feats = df_feature_mean.columns
for i in range(col_num//2):
    fig,axes = plt.subplots(1,2)
    feat_n,feat_r = feats[i*2],feats[i*2+1]
    df_feature_mean[feat_n].plot(ax=axes[0],title=feat_n,figsize=(20,4))
    df_feature_mean[feat_r].plot(ax=axes[1],title=feat_r)


# In[ ]:


# compute the average of all the remaining features and observe the distribution
df_feature_mean = df_clean.groupby('ds')[df_clean.columns[3:-1]].mean()
col_num = df_feature_mean.shape[1]
feats = df_feature_mean.columns
for i in range(col_num//2):
    fig,axes = plt.subplots(1,2)
    feat_n,feat_r = feats[i*2],feats[i*2+1]
    df_feature_mean[feat_n].plot(ax=axes[0],title=feat_n,figsize=(20,4))
    df_feature_mean[feat_r].plot(ax=axes[1],title=feat_r)
# df_feature_mean

# In[ ]:

# Feature augmentation: Observing the image above, we see that some features are incremental and decremental,
# so we create some time window features to expand the features.
# It is very time consuming to generate features every time, so generate them in advance
from tqdm import tqdm

def manu_factors(df_model_id):
    for col in df_model_id:
        if col.startswith(('n_','r_')):
            # 4 dimention, 5 days as time window
            df_model_id[f'{col}_mean'] = df_model_id[col].rolling(5).mean()
            df_model_id[f'{col}_std'] = df_model_id[col].rolling(5).std()
            df_model_id[f'{col}_minmax_diff'] = df_model_id[col].rolling(5).max() - df_model_id[col].rolling(5).min()
            df_model_id[f'{col}_firstlast_diff'] = df_model_id[col].rolling(5).agg(lambda x:x.iloc[-1]-x.iloc[0])
    return df_model_id

tqdm.pandas(desc="feature augment")
df_clean_aug = df_clean.groupby(['disk_id','model']).progress_apply(manu_factors)
#  save the results with augmentation
df_clean_aug.to_csv('/data/df_clean_aug.csv',index=None)


# #### imputation of missing value
#
# - all_neg1: impute with -1
# - all_mean: impute with the mean of all
# - model_mean: impute the missing value according to the mean of each model

# In[ ]:

# load checkpoint
df_clean_aug = pd.read_csv('/data/df_clean_aug/df_clean_aug.csv')


# impute with -1
print('df_clean_aug_na_all_neg1 ...')
df_clean_aug_na_all_neg1 = df_clean_aug.fillna(-1)
df_clean_aug_na_all_neg1.to_csv('/data/df_clean_aug_na_all_neg1.csv',index=None)


# impute with the mean of all SSD
print('df_clean_aug_na_all_mean ...')
df_clean_aug_na_all_mean = df_clean_aug.fillna(df_clean_aug.mean(0))
df_clean_aug_na_all_mean.to_csv('/data/df_clean_aug_na_all_mean.csv',index=None)


# impute with the mean of the each model. If all 0, impute -1
# For the same feature, different SSD model has different distribution
print('df_clean_aug_na_model_mean ...')
def model_mean(df):
    df_fillna = df.fillna(df.mean(0))
    for col in df_fillna:
        if col.startswith(('n_','r_')) and df_fillna[col].isna().sum()==0:
            df_clean_aug[col].fillna(-1,inplace=True)
    return df_fillna
    
df_clean_aug_na_model_mean = df_clean_aug.groupby('model').apply(model_mean)
df_clean_aug_na_all_mean.to_csv('/data/df_clean_aug_na_model_mean.csv',index=None)


df_clean_aug = pd.read_csv('/data/df_clean_aug/df_clean_aug.csv')
df_clean_aug


# In[ ]:





# In[ ]:


df_clean_aug_na_model_mean = pd.read_csv('/data/df_clean_aug/df_clean_aug_na_model_mean.csv')
df_clean_aug_na_model_mean


# In[ ]:


from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import classification_report


# In[ ]:

"""
3. parameter tuning: DT, RF, GBDT 
"""


def flatten_classification_report(clf_report_dict):
    flatten_dict = {}
    for k, v in clf_report_dict.items():
        if isinstance(v, dict):
            for vk, vv in v.items():
                flatten_dict[f"{k}_{vk}"] = vv
        else:
            flatten_dict[k] = v
    return flatten_dict


# In[ ]:


for col in df_clean_aug_na_model_mean.columns:
    if col.endswith(('_mean','_std','_minmax_diff','_firstlast_diff')):
        df_clean_aug_na_model_mean.drop(col,axis=1,inplace=True)
df_clean_aug_na_model_mean.shape


# In[ ]:


# tuning: DT: max_depth
def paramter_opti_dt(X_train, y_train, X_test, y_test):
    result_list = []
    for depth in range(5,30):
        estimator = DecisionTreeClassifier(class_weight=None, criterion='gini', max_depth=depth,
                               max_features=None, max_leaf_nodes=None,
                               min_impurity_decrease=0.0, min_impurity_split=None,
                               min_samples_leaf=1, min_samples_split=2,
                               min_weight_fraction_leaf=0.0, presort=False,
                               random_state=2022, splitter='best')


        estimator.fit(X_train, y_train)
        y_pred = estimator.predict(X_test)
        clf_report_dict = classification_report(y_test, y_pred, output_dict=True)
        result_dict = flatten_classification_report(clf_report_dict)
        result_list.append(result_dict)
    return result_list


# In[ ]:


from matplotlib.pyplot import MultipleLocator

def plot_model(model_precision_list):
    color_list = ['darkorange', 'lightgreen', 'lightcoral', 'lightskyblue', 'peru', 'lightpink']
    axis_x = range(5, 30)
    plt.rcParams['font.sans-serif']=['SimHei']
    plt.rcParams['axes.unicode_minus']=False
    plt.figure(figsize=(10,5))
    plt.figure(1)

    fg1 = plt.subplot(121)
    fg1.set_title('avg_precision of DT, parameter:max depth')
    i = 0
    for model in model_list:
        fg1.plot(axis_x, model_precision_list[model], color=color_list[i], label= model + ' precision')
        i += 1
    fg1.xaxis.set_major_locator(MultipleLocator(2.0))
    fg1.yaxis.set_major_locator(MultipleLocator(0.02))
    fg1.legend()
    fg1.set_xlim(5,30)
    fg1.set_ylim(0.7,1)
    fg1.set_xlabel('max depth')
    fg1.set_ylabel('macro avg_precision')


# In[ ]:


model_list = ["MA1", "MA2", "MB1", "MB2", "MC1", "MC2"]
model_precision_list = {}
for model in model_list:
    df_model = df_clean_aug_na_model_mean[df_clean_aug_na_model_mean.model==model]

    df_test = df_model.sample(frac=0.3, random_state=2022)
    train_idxs = set(df_model.index) - set(df_test.index)
    df_train = df_model.loc[train_idxs]
    feature_used = set(df_model.columns) - set(['disk_id', 'ds', 'model','label'])
    feature_all = feature_used
    X_train,X_test = df_train[feature_used],df_test[feature_used]
    y_train,y_test = df_train['label'],df_test['label']
    
    dict_res = paramter_opti_dt(X_train, y_train, X_test, y_test)
    model_precision_list[model] = [i['macro avg_precision'] for i in dict_res]

plot_model(model_precision_list)


# In[ ]:


plot_model(model_precision_list)


# In[ ]:


# tunning: RF, n_estimators
def paramter_opti_rf(X_train, y_train, X_test, y_test):
    result_list = []
    for estimors in range(10,201,10):
        estimator = RandomForestClassifier(n_estimators=estimors, criterion='entropy', max_depth=4, n_jobs=8, bootstrap=True)

        estimator.fit(X_train, y_train)
        y_pred = estimator.predict(X_test)
        clf_report_dict = classification_report(y_test, y_pred, output_dict=True)
        result_dict = flatten_classification_report(clf_report_dict)
        result_list.append(result_dict)
    return result_list

model_list = ["MA1", "MA2", "MB1", "MB2", "MC1", "MC2"]
model_precision_list = {}
for model in model_list:
    df_model = df_clean_aug_na_model_mean[df_clean_aug_na_model_mean.model==model]

    df_test = df_model.sample(frac=0.3, random_state=2022)
    train_idxs = set(df_model.index) - set(df_test.index)
    df_train = df_model.loc[train_idxs]
    feature_used = set(df_model.columns) - set(['disk_id', 'ds', 'model','label'])
    feature_all = feature_used
    X_train,X_test = df_train[feature_used],df_test[feature_used]
    y_train,y_test = df_train['label'],df_test['label']
    
    dict_res = paramter_opti_rf(X_train, y_train, X_test, y_test)
    model_precision_list[model] = [i['macro avg_precision'] for i in dict_res]


# In[ ]:


color_list = ['darkorange', 'lightgreen', 'lightcoral', 'lightskyblue', 'peru', 'lightpink']
axis_x = range(10, 201, 10)
plt.rcParams['font.sans-serif']=['SimHei']
plt.rcParams['axes.unicode_minus']=False
plt.figure(figsize=(10,5))
plt.figure(1)

fg1 = plt.subplot(121)
fg1.set_title('avg_precision of RF, parameter:n_estimators')
i = 0
for model in model_list:
    fg1.plot(axis_x, model_precision_list[model], color=color_list[i], label= model + ' precision')
    i += 1
fg1.xaxis.set_major_locator(MultipleLocator(20))
fg1.yaxis.set_major_locator(MultipleLocator(0.05))
fg1.legend()
fg1.set_xlim(10,200)
fg1.set_ylim(0.35,1)
fg1.set_xlabel('n_estimators')
fg1.set_ylabel('macro avg_precision')


# In[ ]:


# tunning: GBDT: n_estimators
def paramter_opti_gbdt(X_train, y_train, X_test, y_test):
    result_list = []
    for estimors in range(10,201,10):
        estimator = GradientBoostingClassifier(n_estimators=estimors, learning_rate=0.1, loss='deviance', random_state=2022)

        estimator.fit(X_train, y_train)
        y_pred = estimator.predict(X_test)
        clf_report_dict = classification_report(y_test, y_pred, output_dict=True)
        result_dict = flatten_classification_report(clf_report_dict)
        result_list.append(result_dict)
    return result_list

model_list = ["MA1", "MA2", "MB1", "MB2", "MC1", "MC2"]
model_precision_list = {}
for model in model_list:
    df_model = df_clean_aug_na_model_mean[df_clean_aug_na_model_mean.model==model]

    df_test = df_model.sample(frac=0.3, random_state=2022)
    train_idxs = set(df_model.index) - set(df_test.index)
    df_train = df_model.loc[train_idxs]
    feature_used = set(df_model.columns) - set(['disk_id', 'ds', 'model','label'])
    feature_all = feature_used
    X_train,X_test = df_train[feature_used],df_test[feature_used]
    y_train,y_test = df_train['label'],df_test['label']
    
    dict_res = paramter_opti_gbdt(X_train, y_train, X_test, y_test)
    model_precision_list[model] = [i['macro avg_precision'] for i in dict_res]


# In[ ]:


color_list = ['darkorange', 'lightgreen', 'lightcoral', 'lightskyblue', 'peru', 'lightpink']
axis_x = range(10, 201, 10)
plt.rcParams['font.sans-serif']=['SimHei']
plt.rcParams['axes.unicode_minus']=False
plt.figure(figsize=(10,5))
plt.figure(1)

fg1 = plt.subplot(121)
fg1.set_title('avg_precision of GBDT, parameter:n_estimators')
i = 0
for model in model_list:
    fg1.plot(axis_x, model_precision_list[model], color=color_list[i], label= model + ' precision')
    i += 1
fg1.xaxis.set_major_locator(MultipleLocator(20))
fg1.yaxis.set_major_locator(MultipleLocator(0.05))
fg1.legend()
fg1.set_xlim(10,200)
fg1.set_ylim(0.35,1)
fg1.set_xlabel('n_estimators')
fg1.set_ylabel('macro avg_precision')


# In[ ]:





# In[ ]:



# In[ ]:





# In[ ]:


# try another sampling method
# add the feature selection method from wiki


# # load checkpoint, take a long time
df_clean_aug_na_all_neg1 = pd.read_csv('/data/df_clean_aug_na_all_neg1.csv')
df_clean_aug_na_all_mean = pd.read_csv('/data/df_clean_aug_na_all_mean.csv')
df_clean_aug_na_model_mean = pd.read_csv('/data/df_clean_aug_na_model_mean.csv')

estimator_map = {
    'RF': RandomForestClassifier(n_estimators=60, criterion='entropy',
                                 max_depth=4, n_jobs=8, bootstrap=True, random_state=2022),
    'GBDT': GradientBoostingClassifier(n_estimators=80, learning_rate=0.1,

                                       loss='deviance', random_state=2022),
    'DT': DecisionTreeClassifier(criterion='gini', max_depth=22, min_impurity_decrease=0.0,
                                 min_samples_leaf=1, min_samples_split=2, random_state=2022, splitter='best'),
}

fillna_map = {
    'all_neg1': df_clean_aug_na_all_neg1,
    'all_mean': df_clean_aug_na_all_mean,
    'model_mean': df_clean_aug_na_model_mean,
}

"""
4. Model Training
"""
def feature_select_and_train_eval(
    estimator_map: dict,
    fillna_method:str = 'all_mean',
    train_test_split_method='last_month_as_test',
    feature_aug = True,
    feature_select_method='pearson',
    feature_coref_threshold = 1e-2,
    random_state = 2022,
    pbar=None,
):
    """
    The whole process of modeling and prediction
    
    Args:
        df_fillna_finished: completed pandas DataFrame data of fillna
        fillna_method:  The method of fillna: all_neg1/all_mean/model_mean

        estimator_map:  the dictionary of model training
            key: model name
            value: model


        train_test_split_method: according to the data sampling
            random: random sampling 7:3
            last_month_as_test: predicting the sixth month data based on the first five months data

        feature_aug: True/False, whether augmentation True/False
        
        feature_select_method:  Feature selection methods
            pearson      count Pearson correlation
            spearman     count Spearman's rank correlation
            wiki         use features from wikipedia
            none         reataining all features
        
        feature_coref_threshold:
            if use Pearson or Spearman, set a threshold, remove the features below the threshold

        random_state: random seed
    """

    
    RESULT = []  # record the output

    df_all = fillna_map[fillna_method].copy()
    # if use augmentation, then keep it(mean,std,minmax_diff,firstlast_diff)
    # whether delete the augmentation
    if not feature_aug:
        for col in df_all:
            if col.endswith(('_mean','_std','_minmax_diff','_firstlast_diff')):
                df_all.drop(col,axis=1,inplace=True)


    # according to the feature selection methods, select the features(different SSD model)
    for model in df_all.model.unique():
        print(f"""============================================
        Item model:                 {model}
        fillna_method:              {fillna_method}
        feature_aug:                {feature_aug}
        feature_select:             {feature_select_method} 
        train_test_split_method:    {train_test_split_method} 
        
        """)
        
        # feature selection
        df_model = df_all[df_all.model==model]
        if feature_select_method in ['pearson','spearman']:
            col_coref_map = {}
            for col in df_model:
                if col in ['disk_id', 'ds', 'model','label']:
                    continue
                # count correlation
                coref = df_model[col].corr(df_model['label'], method=feature_select_method)
                # remove the features with same value
                if pd.notna(coref):
                    col_coref_map[col] = coref
            corf_rank = sorted(col_coref_map.items(), key=lambda x:abs(x[1]), reverse=True)
            feature_used = [col for col,coref in corf_rank if coref>=feature_coref_threshold]
            feature_all = corf_rank
        elif feature_select_method == 'wiki':
            # if use feature augmentation, keep the augmentation of features from wiki
            wiki_feature = ('r_5', 'r_10', 'r_184' , 'r_187' , 'r_188' , 'r_196' , 'r_197' , 'r_198' , 'r_201',
                          'n_5', 'n_10', 'n_184' , 'n_187' , 'n_188' , 'n_196' , 'n_197' , 'n_198' , 'n_201' )
            feature_used = [col for col in df_model if col.startswith(wiki_feature)]
            feature_all = feature_used
        # retaining all features
        elif feature_select_method == 'none':
            feature_used = set(df_model.columns) - set(['disk_id', 'ds', 'model','label'])
            feature_all = feature_used

        print(f"feature_used(num: {len(feature_used)}):\n{'/'.join(feature_used)}")
        # training set/test set
        if train_test_split_method=='last_month_as_test':
            mask = (df_model.ds>='2018-12-01')*(df_model.ds<='2018-12-31')
            df_train = df_model[~mask]
            df_test = df_model[mask]

        
        elif train_test_split_method=='random':
            df_test = df_model.sample(frac=0.3, random_state=random_state)
            train_idxs = set(df_model.index) - set(df_test.index)
            df_train = df_model.loc[train_idxs]

        print(f"train_size({len(df_train)})/test_size({len(df_test)})")
        
        X_train,X_test = df_train[feature_used],df_test[feature_used]
        y_train,y_test = df_train['label'],df_test['label']

        # 训练模型
        def flatten_classification_report(clf_report_dict):
            flatten_dict = {}
            for k,v in clf_report_dict.items():
                if isinstance(v,dict):
                    for vk,vv in v.items():
                        flatten_dict[f"{k}_{vk}"] = vv
                else:
                    flatten_dict[k] = v
            return flatten_dict
        

        for estimator_name, estimator in estimator_map.items():
            print(f"-------------> {estimator_name}")
            estimator.fit(X_train, y_train)
            y_pred = estimator.predict(X_test)
            print(classification_report(y_test, y_pred))
            # save result
            clf_report_dict = classification_report(y_test, y_pred, output_dict=True)
            result_dict = flatten_classification_report(clf_report_dict)
            result_dict['model_name'] = estimator_name
            result_dict['feat_select_method'] = feature_select_method
            result_dict['item_model_name'] = model
            result_dict['fillna_method'] = fillna_method
            result_dict['train_test_split_method'] = train_test_split_method
            result_dict['feature_aug'] = feature_aug
            result_dict['feature_used'] = feature_used
            result_dict['feature_number'] = len(feature_used)
            result_dict['feature_all'] = feature_all
            RESULT.append(result_dict)
            pbar.update(1)

    return RESULT


# In[ ]:


#  combine all the parameters
from itertools import product

fillna_methods=['all_neg1', 'all_mean', 'model_mean']
feature_select_methods = ['pearson', 'spearman','wiki','none']
feature_augs = [True, False]
train_test_split_methods=['last_month_as_test','random']
all_params = list(product(fillna_methods,feature_select_methods,feature_augs,train_test_split_methods))
keys = ['fillna_method', 'feature_select_method', 'feature_aug', 'train_test_split_method']
all_params = [dict(zip(keys,vals)) for vals in all_params]


ALL_RESULT = []
for param in tqdm_notebook(all_params,desc='exp ...'):
    result = feature_select_and_train_eval(estimator_map,**param)
    ALL_RESULT.extend(result)


# In[ ]:


#  get the results
df_result = pd.DataFrame(ALL_RESULT)
front_cols = ['item_model_name','fillna_method','feature_aug','feat_select_method','model_name']
df_result = df_result.set_index(front_cols).reset_index()
df_result.to_csv('result.csv',index=None)
df_result


# In[ ]:


df_result=pd.read_csv('result.csv')
# result visulisation
fillna_method = 'all_neg1'
feature_aug = False
for item_model in df_result.item_model_name:
    df_plot = df_result[df_result.item_model_name==item_model]
    mask = (df_plot.feature_aug==feature_aug) * (df_plot.fillna_method==fillna_method)
    df_plot = df_plot[mask]
    sns.barplot(data=df_plot,x='model_name',y='macro avg_f1-score',hue='feat_select_method')
    plt.title(f'{item_model} {fillna_method} feature_aug:{feature_aug}')
    plt.ylim([0.45,0.65])
    plt.show()
    

